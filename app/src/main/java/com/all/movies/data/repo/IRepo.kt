package com.all.movies.data.repo

import com.all.movies.data.MovieInfo

interface IRepo {
	fun searchByName(nm: String): List<MovieInfo>
}