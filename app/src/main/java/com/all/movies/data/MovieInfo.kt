package com.all.movies.data

import android.os.Parcel
import android.os.Parcelable

/**
 * 영화 검색 결과 데이터
 */
data class MovieInfo(
	/** 영화코드를 출력합니다. */
	var movieCd		: String,
	/** 영화명(국문)을 출력합니다. */
	var movieNm		: String,
	/** 영화명(영문)을 출력합니다. */
	var movieNmEn	: String,
	/** 제작연도를 출력합니다. */
	var prdtYear	: String,
	/** 개봉일을 출력합니다. */
	var openDt		: String,
	/** 영화유형을 출력합니다. */
	var typeNm		: String,
	/** 제작상태를 출력합니다. */
	var prdtStatNm	: String,
	/** 제작국가(전체)를 출력합니다. */
	var nationAlt	: String,
	/** 영화장르(전체)를 출력합니다. */
	var genreAlt	: String,
	/** 대표 제작국가명을 출력합니다. */
	var repNationNm	: String,
	/** 대표 장르명을 출력합니다. */
	var repGenreNm	: String,
	/** 영화감독을 나타냅니다. */
	var directors	: String,
	/** 영화감독명을 출력합니다. */
	var peopleNm	: String,
	/** 제작사를 나타냅니다. */
	var companys	: String,
	/** 제작사 코드를 출력합니다. */
	var companyCd	: String,
	/** 제작사명을 출력합니다. */
	var companyNm	: String
): Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:"",
		parcel.readString()?:""
	)

	override fun describeContents(): Int {
		return MovieInfo::class.hashCode()
	}

	override fun writeToParcel(dest: Parcel?, flags: Int) {
		dest?.apply {
			writeString(movieCd)
			writeString(movieNm)
			writeString(movieNmEn)
			writeString(prdtYear)
			writeString(openDt)
			writeString(typeNm)
			writeString(prdtStatNm)
			writeString(nationAlt)
			writeString(genreAlt)
			writeString(repNationNm)
			writeString(repGenreNm)
			writeString(directors)
			writeString(peopleNm)
			writeString(companys)
			writeString(companyCd)
			writeString(companyNm)
		}
	}

	companion object CREATOR : Parcelable.Creator<MovieInfo> {
		override fun createFromParcel(parcel: Parcel): MovieInfo {
			return MovieInfo(parcel)
		}

		override fun newArray(size: Int): Array<MovieInfo?> {
			return arrayOfNulls(size)
		}
	}

}