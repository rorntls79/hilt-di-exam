package com.all.movies.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.all.movies.Const
import com.all.movies.R
import com.all.movies.data.repo.IRepo
import com.all.movies.data.source.AppPref
import com.all.movies.module.ProviderModule
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
	@Inject
	lateinit var repo: IRepo

	@Inject
	lateinit var pref: AppPref

	@Inject
	@ProviderModule.DiStr
	lateinit var diStr: String


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		findViewById<TextView>(R.id.tv_msg)?.let {
			it.text = Const.MESSAGE
		}

//		if( MainActivity::repo.isLateinit ) {
//			Log.e("###", "repo is init")
//		}
//		else {
//			Log.e("###", "repo is NULL")
//		}

		Log.e("###", "PREF HAS \"aa\" ${pref.inst.contains("aa")}")
		pref.inst.edit().putInt("aa", 3).apply()
		Log.e("###", "PREF HAS \"aa\" ${pref.inst.contains("aa")}")

		Log.e("###", "DI give string = $diStr")
	}
}