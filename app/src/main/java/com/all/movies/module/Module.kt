package com.all.movies.module

import android.content.Context
import com.all.movies.data.repo.IRepo
import com.all.movies.data.repo.RestRepo
import com.all.movies.data.source.AppPref
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.android.scopes.ViewScoped
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import javax.inject.Inject
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class BinderModule {

	@Binds
	@Singleton
//	@ViewScoped
	abstract fun bindRestService(
		repo: RestRepo
	): IRepo

//	@Provides
//	@ViewScoped
//	fun provideLocalService(): String = ("1 / 3")

}

@Module
@InstallIn(ActivityComponent::class)
object ProviderModule {
	private const val NM_PREF = "app_pref"

	@Provides
	@ActivityScoped
	fun provLocalPref(
		@ApplicationContext ctx: Context
	): AppPref = AppPref(ctx.getSharedPreferences(NM_PREF, Context.MODE_PRIVATE))


	@Qualifier
	@Retention(AnnotationRetention.BINARY)
	annotation class DiStr

	@Provides
	@ActivityScoped
	@DiStr
	fun provDiString(): String = "Take it STRING, for you"


	@Qualifier
	@Retention(AnnotationRetention.BINARY)
	annotation class HttpInterceptors

	@Provides
	@Singleton
	fun provHttpClient(
		@HttpInterceptors interceptors: List<Interceptor>
	): OkHttpClient {
		return OkHttpClient.Builder().apply {
			for( item in interceptors )
				addInterceptor(item)
		}.build()
	}
}