package com.all.movies.net

import com.all.movies.interceptor.loggingInterceptor
import com.all.movies.module.ProviderModule
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor

@Module
@InstallIn(SingletonComponent::class)
object HttpInterceptorProv {

	@Provides
	@ProviderModule.HttpInterceptors
	fun provInterceptors(): List<Interceptor> {
		return ArrayList<Interceptor>().apply {
			add(loggingInterceptor())
		}
	}
}